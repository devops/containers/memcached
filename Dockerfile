ARG base_image
FROM ${base_image}

ARG memcached_download_url
ARG memcached_download_sha256

COPY ./bin/ /tmp/

RUN /bin/bash -eux /tmp/install $memcached_download_url $memcached_download_sha256

HEALTHCHECK CMD nc -z localhost 11211

EXPOSE 11211

USER memcache

CMD [ "cache-cmd", "run" ]
