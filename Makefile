SHELL = /bin/bash

build_tag ?= memcached

base_image = gitlab-registry.oit.duke.edu/devops/containers/debian-buster:v1

memcached_version = 1.6.9
memcached_download_url = https://memcached.org/files/memcached-$(memcached_version).tar.gz
memcached_download_sha256 = d5a62ce377314dbffdb37c4467e7763e3abae376a16171e613cbe69956f092d1

.PHONY: build
build:
	docker build --pull -t $(build_tag) \
		--build-arg base_image=$(base_image) \
		--build-arg memcached_download_url=$(memcached_download_url) \
		--build-arg memcached_download_sha256=$(memcached_download_sha256) \
		.

.PHONY: test
test:
	build_tag=$(build_tag) ./test.sh
